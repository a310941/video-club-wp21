//Tanto vista, controlador y ruta deben compartir siempre el mismo nombre
//es const porque nunca voy a necesitar moverle a esa constante
const express = require('express');
const { use } = require('../routes/users');


function create(req, res, next) {
  //viene en el cuerpo de la funcion y por eso se usa con body
    let email = req.body.email;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let password = req.body.password;

    let user = new Object({
      name: name,
      lastName: lastName,  
      email:email,
      password:password
    });

    console.log(user);

    //convierte cualquier objeto que mandemos como salida lo regresa en formato json
    res.json (user)
  }

function list(req, res, next) {
  let page = req.params.page ? req.params.page: 0;
    res.send(`Mostrar todos los usuarios del sistema de la pagina ${page}`);
  }

  function index(req, res, next){
    const id = req.params.id;
    res.send(`Mostrar un usuarios del sistema con id = ${id}`);
  }

  function edit(req, res, next){
    const id = req.params.id;
    res.send(`Modificar el usuarios del sistema con id = ${id}`);
  }

  function replace(req, res, next){
    const id = req.params.id;
    res.send(`Reemplazar un usuarios del sistema con id = ${id}`);
  }

  function remove(req, res, next){
    const id = req.params.id;
    res.send(`Remover un usuarios del sistema con id = ${id}`);
  }


  
  module.exports = {
      create, list, index, edit, replace, remove
  }

