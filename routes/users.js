const express = require('express');
const controller =require('../controllers/users')
const router = express.Router();

/* GET users listing. */
/*Esto es una tabla restful */
router.post('/', controller.create);

router.get('/:page?',controller.list);

router.get('/show/:id/:id2', controller.index);

router.put('/:id', controller.edit);

router.patch('/:id', controller.replace);

router.delete('/:id', controller.remove);

//router,get('/registro/:name/:lastName')
module.exports = router;
